﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Transformers
{
    public interface Transformer
    {
        string Name { get; }

        int Health { get; }

        bool IsHumanoid { get; }

        void AddScanner(Scanner scanner);

        void AddWeapon(Weapon weapon);

        void Shot(int damage);
        
        void Move(int speed);

        bool FindEnemy();

        void Fire();

        void Transform();
    }

}
