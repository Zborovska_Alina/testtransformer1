﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Transformers
{
    class TransformerBattleField : Field, FieldSetup
    {
        private readonly Dictionary<object, int> field = new Dictionary<object, int>();

        public void AddUnit(object unit, int pos)
        {
            field.Add(unit, pos);
        }

        public Transformer FindAny(Transformer owner, Scanner scanner)
        {
            int pos;
            if (field.TryGetValue(owner, out pos))
            {
                foreach (var p in field)
                {
                    if (p.Value != pos && scanner.CanScanTargetFrom(p.Value, pos))
                    {
                        return (Transformer) p.Key;
                    }
                }
            }
            return null;
        }

        public void MoveUnit(object unit, int shift)
        {
            int pos;
            if (field.TryGetValue(unit, out pos))
            {
                field[unit] = pos + shift;
            }
        }
    }
}
