using System.Collections.Generic;

namespace Transformers
{
    class TransformersInventory: Inventory
    {
        private List<Transformer> transformers = new List<Transformer>();
        
        private Dictionary<Transformer, List<Scanner>> scannersByTransformers = new Dictionary<Transformer, List<Scanner>>();

        private List<Weapon> weapons = new List<Weapon>();

        public void Setup(Field battleField)
        {
            var optimus = new GroundTransformer("Optimus Prime", battleField);
            var laserbeak = new AirTransformer("Laserbeak", battleField);
            var seaspray = new WaterTransformer("Seaspray", battleField);

            transformers = new List<Transformer>();
            transformers.Add(optimus);
            transformers.Add(laserbeak);
            transformers.Add(seaspray);

            var sonars = new List<Scanner>();
            sonars.Add(new Sonar("SN20", 20));
            sonars.Add(new Sonar("SNX25", 25));

            var opticalScanners = new List<Scanner>();
            opticalScanners.Add(new OpticalScanner("OS10", 20));
            opticalScanners.Add(new OpticalScanner("OSX15", 30));

            var strangerScanners = new List<Scanner>();
            strangerScanners.Add(new StrangerScanner("SS 3", 15));
            strangerScanners.Add(new StrangerScanner("SS 5", 25));

            scannersByTransformers.Add(optimus, opticalScanners);
            scannersByTransformers.Add(laserbeak, strangerScanners);
            scannersByTransformers.Add(seaspray, sonars);

            weapons.Add(new Gun("G10"));
            weapons.Add(new Gun("G15"));
            weapons.Add(new Pistol("P10"));
            weapons.Add(new Pistol("P12"));
        }

        public List<Transformer> ListTransformers()
        {
            return transformers;
        }

        public List<Scanner> ListScannersFor(Transformer t)
        {
            return scannersByTransformers.GetValueOrDefault(t, new List<Scanner>());
        }

        public  List<Weapon> ListWeaponsFor(Transformer t)
        {
            return weapons;
        }
    }
}