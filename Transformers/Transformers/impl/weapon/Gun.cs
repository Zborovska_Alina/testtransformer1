﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Transformers
{
    public class Gun : BaseWeapon
    {
        private readonly string model;

        public Gun(string model): base()
        {
            this.model = model;
        }

        public override string ToString()
        {
            return $"Gun {model}";
        }
    }
}
