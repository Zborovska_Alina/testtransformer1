using System;
using Xunit;

namespace Transformers.Tests
{
    public class TransformerTest
    {
        private static readonly TestField field = new TestField();
        Transformer transformer;

        public TransformerTest()
        {
            transformer = new AirTransformer("TestT", field);
            transformer.AddScanner(new OpticalScanner("TS10", 10));
            transformer.AddWeapon(new Gun("TG12"));
        }

        [Fact]
        public void TransformerCanTransform()
        {
            transformer.Transform();

            Assert.True(transformer.IsHumanoid);
        }

        [Fact]
        public void TransformerCanMove()
        {
            transformer.Move(2);

            Assert.Equal(2, field.Shift);
        }

        [Fact]
        public void TransformerCanFindEnemyWithScanner()
        {
            Assert.True(transformer.FindEnemy());
        }

        [Fact]
        public void TransformerDoesNotFireWhenItIsTransport()
        {
            transformer.FindEnemy();
            transformer.Fire();

            Assert.Equal(100, field.FoundEnemy.Health);
        }

        [Fact]
        public void TransformerFiresWhenItIsHumanoid()
        {
            transformer.FindEnemy();
            transformer.Transform();
            transformer.Fire();

            Assert.True(field.FoundEnemy.Health < 100);
        }
    }
}
